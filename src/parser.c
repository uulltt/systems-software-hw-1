/*
 * COP3402 - Spring 2018
 * System Software Assignment 3
 * Submitted by: Aaron Varkonyi, William White
 */

// includes
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "main.h"

const char* const OP_CODES[] = {
	"err", "lit", "rtn", "lod", "sto", "cal",
	"inc", "jmp", "jpc", "sio", "neg", "add",
	"sub", "mul", "div", "odd", "mod", "eql",
	"neq", "lss", "leq", "gtr", "geq"
};

Token token;
char ident[12];
int number;

Symbol symbols[MAX_SYMBOL_TABLE_SIZE];
int amtSymbols = 0;

Instruction code[MAX_CODE_LENGTH];
int amtCode = 0;

Lex lexTable[];
int lexTableSize;

int sp = 1;
int regPtr = 0;
int lexLevel = 1;
int level = 1;

int getSymbolIndex(char* name) {
	int i;
	for (i = 0; i < amtSymbols; i++) {
		if (strcmp(name, symbols[i].name) == 0) {
			return i;
		}
	}
	return -1;
}

void addSymbol(int type, char name[], int val, int addr) {
	symbols[amtSymbols].type = type;
	strcpy(symbols[amtSymbols].name, name);
	symbols[amtSymbols].val = val;
	symbols[amtSymbols].addr = addr;
	amtSymbols++;
}

void emit(int OP, int R, int L, int M) {
	code[amtCode].OP = OP;
	code[amtCode].R = R;
	code[amtCode].L = L;
	code[amtCode].M = M;
	amtCode++;
}

void expression();
int getToken();

char* getErrorMsg(int code) {
	switch(code) {
		case 1:  return "Use = instead of :="; break;
		case 2:  return "= must be followed by a number"; break;
		case 3:  return "Identifier must be followed by ="; break;
		case 4:  return "Const, int, procedure must be followed by identifier"; break;
		case 5:  return "Semicolon or comma missing"; break;
		case 6:  return "Incorrect symbol after procedure declaration"; break;
		case 7:  return "Statement expected"; break;
		case 8:  return "Incorrect symbol after statement part in block"; break;
		case 9:  return "Period expected"; break;
		case 10: return "Semicolon between statements missing"; break;
		case 11: return "Undeclared identifier"; break;
		case 12: return "Assignment to constant or procedure is not allowed"; break;
		case 13: return "Assignment operator expected"; break;
		case 14: return "Call must be followed by an identifier"; break;
		case 15: return "Call of a constant or variable is meaningless"; break;
		case 16: return "Then expected"; break;
		case 17: return "Semicolon or } expected"; break;
		case 18: return "Do expected"; break;
		case 19: return "Incorrect symbol following statement"; break;
		case 20: return "Relational operator expected"; break;
		case 21: return "Expression must not contain a procedure identifier"; break;
		case 22: return "Right parenthesis missing"; break;
		case 23: return "The preceding factor cannot begin with this symbol"; break;
		case 24: return "Expression cannot begin with this symbol"; break;
		case 25: return "Number is too large"; break;
		case 26: return "End expected"; break;
		case 27: return "All available registers in use"; break;
		case 28: return "Variable not initialized"; break;
		case 29: return "Identifier expected after read or write"; break;
		default: return "Unknown error code!"; break;
	}
}

void error(int code) {
	printf("Error: %s\n", getErrorMsg(code));
	exit(1);
}

void assert(int value, int code) {
	if (!value) {
		error(code);
	}
}

void factor() {
	if (token.type == identsym) {
		int index = getSymbolIndex(token.value);
		assert(index > -1, 11);
		if (symbols[index].type == VAR) {
			emit(LOD, regPtr, lexLevel - symbols[index].level, symbols[index].addr);
		} else if (symbols[index].type == CONST) {
			emit(LIT, regPtr, 0, symbols[index].val);
		}
		regPtr++;
		getToken();
	} else if (token.type == numbersym) {
		emit(LIT, regPtr, 0, atoi(token.value));
		regPtr++;
		getToken();
	} else if (token.type == lparentsym) {
		getToken();
		expression();
		assert(token.type == rparentsym, 22);
		getToken();
	} else {
		error(23);
	}
}

void term() {
	factor();
	while (token.type == multsym || token.type == slashsym) {
		int type = token.type;
		getToken();
		factor();
		if (type == multsym) {
			emit(MUL, regPtr - 2, regPtr - 2, regPtr - 1);
		} else if (type == slashsym) {
			emit(DIV, regPtr - 2, regPtr - 2, regPtr - 1);
		}
		regPtr--;
	}
}

void expression() {
	if (token.type == plussym || token.type == minussym) {
		int type = token.type;
		getToken();
		term();
		if (type == minussym) {
			emit(NEG, regPtr - 1, regPtr - 1, 0);
		}
	} else {
		term();
	}

	while (token.type == plussym || token.type == minussym) {
		int type = token.type;
		getToken();
		term();
		if (type == plussym) {
			emit(ADD, regPtr - 2, regPtr - 2, regPtr - 1);
			regPtr--;
		} else if (type == minussym) {
			emit(SUB, regPtr - 2, regPtr - 2, regPtr - 1);
			regPtr--;
		}
	}
}

void condition() {
	if (token.type == oddsym) {
		getToken();
		expression();
		emit(ODD, regPtr - 1, regPtr - 1, 0);
	} else {
		expression();
		int op;
		switch(token.type) {
			case eqsym: op = EQL; break;
			case neqsym: op = NEQ; break;
			case lessym: op = LSS; break;
			case leqsym: op = LEQ; break;
			case gtrsym: op = GTR; break;
			case geqsym: op = GEQ; break;
			default: error(20); break;
		}
		getToken();
		expression();
		emit(op, regPtr - 2, regPtr - 2, regPtr - 1);
		regPtr--;
	}
}

void statement() {
	if (token.type == identsym) {
		int index = getSymbolIndex(token.value);
		assert(index > -1, 11);
		assert(symbols[index].type != CONST, 12);
		getToken();
		assert(token.type == becomessym, 3);
		getToken();
		expression();
		emit(STO, regPtr - 1, lexLevel - symbols[index].level, symbols[index].addr);
		regPtr--;
	} else if (token.type == callsym) {
		getToken();
		int index = getSymbolIndex(token.value);
		assert(index > -1, 11);
		if (symbols[index].type == PROC) {
			emit(CAL, 0, level, symbols[index].addr);
		} else {
			error(15);
		}
		assert(token.type == identsym, 14);
		getToken();
	} else if (token.type == beginsym) {
		do {
		getToken();
		statement();
		}while (token.type == semicolonsym);
		assert(token.type == endsym, 26);
		getToken();
	} else if (token.type == ifsym) {


		getToken();
		condition();
		assert(token.type == thensym, 16);
		getToken();
		int c = amtCode;
		emit(JPC, regPtr - 1, 0, 0);
		regPtr--;
		statement();
		if (token.type == elsesym) {
			int c2 = amtCode;
			emit(JMP, 0, 0, 0);
			code[c].M = amtCode;
			getToken();
			statement();
			code[c2].M = amtCode;
		} else {
			code[c].M = amtCode;
		}
	} else if (token.type == whilesym) {
		int c = amtCode;
		getToken();
		condition();
		int c2 = amtCode;
		emit(JPC, regPtr - 1, 0, 0);
		regPtr--;
		assert(token.type == dosym, 18);
		getToken();
		statement();
		emit(JMP, 0, 0, c);
		code[c2].M = amtCode;
	} else if (token.type == readsym) {
		getToken();
		assert(token.type == identsym, 29);
		int index = getSymbolIndex(token.value);
		assert(index > -1, 11);
		emit(SIO, regPtr, 0, 2);
		if (symbols[index].type == VAR) {
			emit(STO, regPtr, lexLevel - symbols[index].level, symbols[index].addr - 1);
		} else if (symbols[index].type == CONST) {
			error(12);
		}
		getToken();
	} else if (token.type == writesym) {
		getToken();
		assert(token.type == identsym, 29);
		int index = getSymbolIndex(token.value);
		assert(index > -1 && symbols[index].type != PROC, 11);
		if (symbols[index].type == CONST) {
			emit(LIT, regPtr, 0, symbols[index].val);
			emit(SIO, regPtr, 0, 1);
		} else if (symbols[index].type == VAR) {

			emit(LOD, regPtr, lexLevel - symbols[index].level, symbols[index].addr);
			emit(SIO, regPtr, 0, 1);
		}
		getToken();
	}
}

void block() {
	sp += 3;
	int space = 4;
	int jumpAddr = amtCode;
	if (token.type == constsym) {
		do {
			getToken();
			assert(token.type == identsym, 4);
			char name[12];
			strcpy(name, token.value);
			getToken();
			assert(token.type == eqsym, 3);
			getToken();
			assert(token.type == numbersym, 2);
			addSymbol(CONST, name, atoi(token.value), 0);
			getToken();
		} while (token.type == commasym);
		assert(token.type == semicolonsym, 5);
		getToken();
	}
	if (token.type == varsym) {
		do {
			getToken();
			assert(token.type == identsym, 4);
			char name[12];
			strcpy(name, token.value);
			addSymbol(VAR, name, 0, sp);
			symbols[amtSymbols - 1].level = level;
			space++;
			sp++;
			getToken();
		} while (token.type == commasym);
		assert(token.type == semicolonsym, 5);
		getToken();
	}
	while (token.type == procsym) {
		getToken();
		assert(token.type == identsym, 4);
		char name[12];
		strcpy(name, token.value);
		addSymbol(PROC, name, 0, 0);
		symbols[amtSymbols - 1].level = level;
		symbols[amtSymbols - 1].addr = jumpAddr + 1;
		getToken();
		assert(token.type == semicolonsym, 17);
		getToken();
		level++;
		block();
		getToken();
	}
	code[jumpAddr].M = amtCode;
	emit(INC, 0, 0, space);
	statement();
	lexLevel--;
}

void program() {
	getToken();
	block();
	assert(token.type == periodsym, 9);
	emit(SIO, 0, 0, 3);
}

int lexIndex = 0;
int getToken() {
	if (lexIndex < lexTableSize) {
		token.type = lexTable[lexIndex].token;
		if (token.type == identsym || token.type == numbersym) {
			strcpy(token.value, lexTable[lexIndex].name);
		} else {
			token.value[0] = '\0';
		}
		lexIndex++;
		return 1;
	} else {
		token.type = -1;
		token.value[0] = '\0';
		lexIndex++;
		return 0;
	}
}

void parse() {
	int i;
	for (i = 0; i < MAX_CODE_LENGTH; i++) {
		code[i].OP = 0;
		code[i].R = 0;
		code[i].L = 0;
		code[i].M = 0;
	}

	program();

	if (PRINT_ASSEMBLY) {
		printf("Code is syntactically correct. Assembly code generated successfully.\n");
		printf("-------------------------------------------\n");
		printf("GENERATED INTERMEDIATE CODE:\n");
		for (i = 0; i < amtCode; i++) {
			printf("%3d %s %d %d %d\n", i, OP_CODES[code[i].OP], code[i].R, code[i].L, code[i].M);
		}
		printf("\n");
	}
}