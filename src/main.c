/*
 * COP3402 - Spring 2018
 * System Software Assignment 3
 * Submitted by: Aaron Varkonyi, William White
 */

// includes
#include <stdlib.h>
#include <stdio.h>

#include "main.h"

int PRINT_LEXEME = 0;
int PRINT_ASSEMBLY = 0;
int PRINT_VM = 0;

// constants
const char FLAG_START = '-';
const char PRINT_LEXEME_FLAG = 'l';
const char PRINT_ASSEMBLY_FLAG = 'a';
const char PRINT_VM_TRACE_FLAG = 'v';

FILE* getFile(char* name, char* perm) {
	FILE* file = fopen(name, perm);
	if (file == NULL) {
		printf("Error: Could not open file! ( %s )\n", name);
		exit(1);
	}
	return file;
}

// main program
int main(int argc, char** argv) {

	int foundFileName = 0;
	char* fileName;

	// parse arguments
	int i;
	for (i = 1; i < argc; i++) {
		// is flag
		if (argv[i][0] == FLAG_START) {
			int j = 1;
			while (argv[i][j] != '\0') {
				PRINT_LEXEME |= argv[i][j] == PRINT_LEXEME_FLAG;
				PRINT_ASSEMBLY |= argv[i][j] == PRINT_ASSEMBLY_FLAG;
				PRINT_VM |= argv[i][j] == PRINT_VM_TRACE_FLAG;
				j++;
			}
		} else if (!foundFileName) {
			foundFileName = 1;
			fileName = argv[i];
		}
	}

	// now use other files
	lex(fileName);
	parse();
	vm();

	return 0;
}
