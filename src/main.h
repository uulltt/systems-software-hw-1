/*
 * COP3402 - Spring 2018
 * System Software Assignment 3
 * Submitted by: Aaron Varkonyi, William White
 */

// header guard
#ifndef MAIN_H
#define MAIN_H

// CLI flags
extern int PRINT_LEXEME;
extern int PRINT_ASSEMBLY;
extern int PRINT_VM;

// static variables
#define MAX_STACK_HEIGHT 2000
#define MAX_CODE_LENGTH 500
#define MAX_LEXI_LEVELS 3

#define SYMBOL_COUNT 17
#define SYM_COUNT 14
#define WORD_COUNT 13
#define INVALID_COUNT 8

#define MAX_SYMBOL_TABLE_SIZE 100
#define LEX_TABLE_SIZE 5000

// enums
enum {
	nulsym = 1, identsym, numbersym, plussym, minussym, multsym,
	slashsym, oddsym, eqsym, neqsym, lessym, leqsym, gtrsym, geqsym,
	lparentsym, rparentsym, commasym, semicolonsym, periodsym,
	becomessym, beginsym, endsym, ifsym, thensym, whilesym, dosym,
	callsym, constsym, varsym, procsym, writesym, readsym, elsesym
};

enum {
	LIT = 1, RTN, LOD, STO, CAL, INC, JMP, JPC, SIO, NEG, ADD,
	SUB, MUL, DIV, ODD, MOD, EQL, NEQ, LSS, LEQ, GTR, GEQ,
};

enum {
	CONST = 1, VAR, PROC
};

// structs
typedef struct {
	int OP; // operation code
	int R;  // register
	int L;  // lexicographical level
	int M;  // argument
} Instruction;

typedef struct {
	char* name;
	int token;
} Lex;

typedef struct {
	int type;
	char name[12];
	int val;
	int level;
	int addr;
} Symbol;

typedef struct {
	int type;
	char value[12];
} Token;

// shared memory
extern Lex lexTable[LEX_TABLE_SIZE];
extern int lexTableSize;
extern Instruction code[MAX_CODE_LENGTH];

// utility functions
FILE* getFile(char* name, char* perm);

// main functions
void lex();
void parse();
void vm();

#endif // MAIN_H