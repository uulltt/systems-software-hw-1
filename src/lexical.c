/*
 * COP3402 - Spring 2018
 * System Software Assignment 2
 * Submitted by: Aaron Varkonyi, William White
 */

// includes
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "main.h"

Lex lexTable[];
int lexTableSize;

// special symbols
const char* const reswords[WORD_COUNT] = {"begin", "end", "if", "then", "while", "do", "call", "const", "var", "procedure", "write", "read", "else"};
const char spsym[SYM_COUNT] = {'+', '-', '*', '/', '%', '(', ')', '=', ',', '.', '<', '>', ';', ':'};
const char invalids[INVALID_COUNT] = {'$', '#', '&', '!', '_', '~', '`', '?'};
const char* const spsymstr[SYMBOL_COUNT] = {"+", "-", "*", "/", "%", "=", "<>", "<", "<=", ">", ">=", "(", ")", ",", ";", ".", ":="};
const char* const SYMBOL_NAMES[] = {
	"invalidsym", "nulsym", "identsym", "numbersym", "plussym", "minussym", "multsym",
	"slashsym", "oddsym", "eqsym", "neqsym", "lessym", "leqsym", "gtrsym", "geqsym",
	"lparentsym", "rparentsym", "commasym", "semicolonsym", "periodsym",
	"becomessym", "beginsym", "endsym", "ifsym", "thensym", "whilesym", "dosym",
	"callsym", "constsym", "varsym", "procsym", "writesym", "readsym", "elsesym"
};


void printSource(FILE* file) {
	printf("Source Program:\n");
	int c = fgetc(file);
	while (c != EOF) {
		printf("%c", c);
		c = fgetc(file);
	}
}

char* substring(char* original, int start, int len) {
	char* temp = (char*) malloc(len);
	int c = 0;
	while (c < len) {
		temp[c] = *(original + c + start);
		c++;
	}
	temp[c] = '\0';
	return temp;
}

int getLexToken(Lex line) {
	// certain words
	int i;
	for (i = 0; i < WORD_COUNT; i++) {
		if (strcmp(line.name, reswords[i]) == 0) {
			return i + 21;
		}
	}
	// special symbols
	for (i = 0; i < SYMBOL_COUNT; i++) {
		if (strcmp(line.name, spsymstr[i]) == 0) {
			return i + 4;
		}
	}

	if (strcmp(line.name, "odd") == 0) {
		return 8;
	}
	int c;
	int digitcount = 0;
	for (c = 0; line.name[c] != '\0'; c++) {
		if (isdigit(line.name[c])) {
			digitcount++;
		}
	}
	if (digitcount == c) {
		return 3;
	}
	for (c = 0; line.name[c] != '\0'; c++) {
		for (i = 0; i < SYM_COUNT; i++) {
			if (line.name[c] == spsym[i]) {
				return 1;
			}
		}
	}
	return 2;
}

void lex(char* fileName) {
	// fetch/create files
	FILE* inputFile = getFile(fileName, "r");

	// read input
	int lexTableIndex;
	int ignore = 0;
	for (lexTableIndex = 0; !feof(inputFile); lexTableIndex++) {
		if (lexTableIndex > 0) {
			if (strlen(lexTable[lexTableIndex - 1].name) == 0) {
				lexTableIndex--;
			}
		}
		lexTable[lexTableIndex].name = (char*) calloc(500, sizeof(char));

		if (!ignore) {
			if (fscanf(inputFile, "%11s", lexTable[lexTableIndex].name)) {
				if (lexTableIndex > 0) {
					if (strlen(lexTable[lexTableIndex].name) == 0) {
						break;
					}
				}
			}

			// ignoring comments
			if (strlen(lexTable[lexTableIndex].name) > 1 && lexTable[lexTableIndex].name[0] == '/' && lexTable[lexTableIndex].name[1] == '/') {
				ignore = 1;
				lexTableIndex--;
				continue;
			} else if (strlen(lexTable[lexTableIndex].name) > 1 && lexTable[lexTableIndex].name[0] == '/' && lexTable[lexTableIndex].name[1] == '*') {
				ignore = 2;
				lexTableIndex--;
				continue;
			}
		} else {
			if (ignore == 1) {
				int a;
				do {
					a = fgetc(inputFile);
				} while (a != '\n' && a != '\r' && a != EOF);
			} else {
				int a, b;
				do {
					b = a;
					a = fgetc(inputFile);
				} while (a != '/' && b != '*' && a != EOF);
			}
			ignore = 0;
			continue;
		}

		// checking for invalid symbols
		int c;
		for (c = 0; *((lexTable[lexTableIndex].name) + c) != '\0'; c++) {
			int i;
			for (i = 0; i < INVALID_COUNT; i++) {
				if (*((lexTable[lexTableIndex].name) + c) == invalids[i]) {
					printf("Error: Invalid Symbol.");
					exit(1);
				}
			}
		}
		c = 0;

		int s;
		for (s = 0; s < SYMBOL_COUNT; s++) {
			if (strcmp(lexTable[lexTableIndex].name, spsymstr[s]) == 0) {
				break;
			}
		}

		if (s == SYMBOL_COUNT) {
			char* oldname = lexTable[lexTableIndex].name;
			int len;
			do {
				char firstchar = lexTable[lexTableIndex].name[0];
				if (isalnum(firstchar)) {
					for (c = 1; *((lexTable[lexTableIndex].name) + c) != '\0'; c++) {
						int i;
						for (i = 0; i < SYM_COUNT; i++) {
							if (*((lexTable[lexTableIndex].name) + c) == spsym[i]) {
								oldname = lexTable[lexTableIndex].name;
								len = (strlen(oldname) - c);
								lexTable[lexTableIndex].name = substring(oldname, 0, c);
								lexTable[lexTableIndex].token = getLexToken(lexTable[lexTableIndex]);
								int token = getLexToken(lexTable[lexTableIndex]);
								if ((token == 2 && (strlen(lexTable[lexTableIndex].name) > 11 || (isdigit(firstchar)))) || (token == 3 && strlen(lexTable[lexTableIndex].name) > 5)) {
									if (token == 3) {
										printf("Error: Number too large.");
									}
									if (token == 2) {
										if (isdigit(firstchar)) {
											printf("Error: Invalid identifier.");
										} else {
											printf("Error: Identifier too long.");
										}
									}
									exit(1);
								}
								lexTableIndex++;
								lexTable[lexTableIndex].name = (char*) malloc(len + 1);
								char* str = substring(oldname, c, len);
								strncpy(lexTable[lexTableIndex].name, str, len + 1);
								free(str);
								oldname = lexTable[lexTableIndex].name;
								break;
							}
						}
						if (i < SYM_COUNT) {
							break;
						}
					}
				}

				firstchar = lexTable[lexTableIndex].name[0];
				if (!isalnum(firstchar)) {
					int i;
					for (i = 0; i < SYMBOL_COUNT; i++) {
						int c = strlen(spsymstr[i]);
						char* str = substring(lexTable[lexTableIndex].name, 0, c);
						if (strcmp(str, spsymstr[i]) == 0 && c < strlen(lexTable[lexTableIndex].name)) {
							// checking to make sure there's not a = after the substring we're looking at if we are looking at < or >
							if ((i != 7 && i != 9) || lexTable[lexTableIndex].name[c] != '=') {
								oldname = lexTable[lexTableIndex].name;
								int len = (strlen(oldname) - c);

								lexTable[lexTableIndex].name = substring(oldname, 0, c);
								lexTable[lexTableIndex].token = getLexToken(lexTable[lexTableIndex]);

								lexTableIndex++;
								lexTable[lexTableIndex].name = (char*) malloc(len + 1);
								char* str2 = substring(oldname, c, len);
								strncpy(lexTable[lexTableIndex].name, str2, len + 1);
								free(str2);
								oldname = lexTable[lexTableIndex].name;
								break;
							}
						}
						free(str);
					}
					if (i == SYMBOL_COUNT) {
						break;
					}
				}
			} while (getLexToken(lexTable[lexTableIndex]) == 1);
		}
		lexTable[lexTableIndex].token = getLexToken(lexTable[lexTableIndex]);
		int token = getLexToken(lexTable[lexTableIndex]);
		if ((token == identsym && (strlen(lexTable[lexTableIndex].name) > 11 || isdigit(lexTable[lexTableIndex].name[0]))) || (token == numbersym && strlen(lexTable[lexTableIndex].name) > 5)) {
			if (token == numbersym) {
				printf("Error: Number too large.");
			}
			if (token == identsym) {
				if (isdigit(lexTable[lexTableIndex].name[0])) {
					printf("Error: Invalid identifier.");
				} else {
					printf("Error: Identifier too long.");
				}
			}
			exit(1);
		}
	}
	fclose(inputFile);

	lexTableSize = lexTableIndex;

	if (PRINT_LEXEME) {
		printf("-------------------------------------------\n");
		printf("LIST OF LEXEMES/TOKENS:\n\n");
		printf("Internal Representation:\n");
		int i;
		for (i = 0; i < lexTableSize; i++) {
			printf("%d", lexTable[i].token);
			if (lexTable[i].token == identsym || lexTable[i].token == numbersym) {
				printf(" %s", lexTable[i].name);
			}
			printf("  ");
		}
		printf("\n\n");

		printf("Symbolic Representation:\n");
		for (i = 0; i < lexTableSize; i++) {
			printf("%s", SYMBOL_NAMES[lexTable[i].token]);
			if (lexTable[i].token == identsym || lexTable[i].token == numbersym) {
				printf(" %s", lexTable[i].name);
			}
			printf("  ");
		}
		printf("\n\n\n");
	}
}