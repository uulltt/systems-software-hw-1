/*
 * COP3402 - Spring 2018
 * System Software Assignment 1
 * Submitted by: Aaron Varkonyi, William White
 */

// includes
#include <stdlib.h>
#include <stdio.h>

#include "main.h"

const char* const OP_CODE_NAMES[] = {
	"err", "lit", "rtn", "lod", "sto", "cal",
	"inc", "jmp", "jpc", "sio", "neg", "add",
	"sub", "mul", "div", "odd", "mod", "eql",
	"neq", "lss", "leq", "gtr", "geq"
};

// variables
int SP = 0;
int BP = 1;
int PC = 0;
int IR = 0;
int lexLvl = 0;
int R[16] = {0};
int stack[MAX_STACK_HEIGHT] = {0, 0, 1};
int halt = 0;
Instruction code[MAX_CODE_LENGTH];

// utility functions
int base(int l, int base) {
	int b1; // find base L levels down
	b1 = base;
	while (l > 0) {
		b1 = stack[b1 + 1];
		l--;
	}
	return b1;
}

void printStack(int sp, int bp, int* stack, int lexLvl) {
	int i;
	if (bp == 1) {
		if (lexLvl > 0) {
		printf("|");
		}
	} else {
		// print the lesser lexical level
		printStack(bp - 1, stack[bp + 2], stack, lexLvl - 1);
		printf("|");
	}
	// print the stack contents - at the current level
	for (i = bp; i <= sp; i++) {
		printf("%d ", stack[i]);
	}
}

void vm() {

	printf("-------------------------------------------\n");
	if (PRINT_VM) {
		printf("VIRTUAL MACHINE TRACE:\n");
		printf("Initial Values:\n");
		printf("PC\tBP\tSP\tStack\n");
		printf("%d\t%d\t%d\t%d \n", PC, BP, SP, 0);
		printf("\n");
		printf("Stack Trace:\n");
		printf("\t\tR\tL\tM\tPC\tBP\tSP\tStack\n");
	} else {
		printf("PROGRAM INPUT/OUTPUT:\n");
	}

	// main program loop
	while (!halt) {
		// fetch cycle
		IR = PC;
		PC++;

		// mirror the assignment pdf's variables
		int L = code[IR].L;
		int M = code[IR].M;

		int i = code[IR].R;
		int j = code[IR].L;
		int k = code[IR].M;

		// execute cycle
		switch (code[IR].OP) {
			case LIT:
				R[i] = M;
				break;
			case RTN:
				SP = BP - 1;
				BP = stack[SP + 3];
				PC = stack[SP + 4];
				lexLvl--;
				break;
			case LOD:
				R[i] = stack[base(L, BP) + M];
				break;
			case STO:
				stack[base(L, BP) + M] = R[i];
				break;
			case CAL:
				stack[SP + 1] = 0;			 // space to return value
				stack[SP + 2] = base(L, BP); // static link (SL)
				stack[SP + 3] = BP;			 // dynamic link (DL)
				stack[SP + 4] = PC;			 // return address (RA)
				BP = SP + 1;
				SP = SP + 4;
				PC = M;
				lexLvl++;
				break;
			case INC:
				SP = SP + M;
				break;
			case JMP:
				PC = M;
				break;
			case JPC:
				if (R[i] == 0) {
					PC = M;
				}
				break;
			case SIO:
				switch (M) {
				case 1:
					printf("OUTPUT: %d\n", R[i]);
					break;
				case 2:
					printf("\nEnter number: ");
					scanf("%d", &R[i]);
					break;
				case 3:
					halt = 1;
					break;
				}
				break;
			case NEG:
				R[i] = -R[j];
				break;
			case ADD:
				R[i] = R[j] + R[k];
				break;
			case SUB:
				R[i] = R[j] - R[k];
				break;
			case MUL:
				R[i] = R[j] * R[k];
				break;
			case DIV:
				R[i] = R[j] / R[k];
				break;
			case ODD:
				R[i] = R[j] % 2;
				break;
			case MOD:
				R[i] = R[j] % R[k];
				break;
			case EQL:
				R[i] = R[j] == R[k];
				break;
			case NEQ:
				R[i] = R[j] != R[k];
				break;
			case LSS:
				R[i] = R[j] < R[k];
				break;
			case LEQ:
				R[i] = R[j] <= R[k];
				break;
			case GTR:
				R[i] = R[j] > R[k];
				break;
			case GEQ:
				R[i] = R[j] >= R[k];
				break;
		}

		if (halt) {
			PC = 0;
			SP = 0;
		}

		if (PRINT_VM) {
			printf("%d\t%s\t%d\t%d\t%d\t%d\t%d\t%d\t", IR, OP_CODE_NAMES[code[IR].OP], i, j, k, PC, BP, SP);
			if (halt) {
				printf("%d ", 0);
			} else {
				printStack(SP, BP, stack, lexLvl);
			}
			printf("\n");
			printf("RF: ");
			for (i = 0; i < 8; i++) {
				printf("%d ", R[i]);
			}
			printf("\n");
		}
	}
	printf("\n");
	printf("Finished execution. Exiting...\n");
}