all: compile

compile:
	gcc -o compile src/*.c

clean:
	rm compile

run: compile
	./compile input -lav > output

crun: clean run

.PHONY: clean